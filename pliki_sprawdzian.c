#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Wyraz
{
	char *slowo;
	int krotnosc;
};

typedef struct LIST List;
struct LIST
{
	List *next;
	struct Wyraz wyraz;
};

void readFile(List **head)
{
	FILE *plik=NULL;
	char bufor[30];
	List *nowy, *pom;
	plik =fopen("tekst.txt","r");
	if (plik == NULL)
	{
		printf("nie udalo sie otworzyc pliku tekst.txt");
	}
	else
	{
		while(fscanf(plik,"%s",bufor) != EOF)
		{
			nowy = (List*)malloc(sizeof(List));
			nowy->next=NULL;
			nowy->wyraz.slowo=(char*)malloc(sizeof(char)*(strlen(bufor)+1));
			strcpy(nowy->wyraz.slowo,bufor);
			if (!(nowy->wyraz.krotnosc))
				nowy->wyraz.krotnosc=1;
			else
				nowy->wyraz.krotnosc+=1;
			if (*head == NULL)
				*head = nowy;
			else
			{
				pom = *head;
				while (pom->next != NULL)
					pom = pom->next;
				pom->next = nowy;
			}
		}
	}
	fclose(plik);
}

void PrintList(List *head){
    while(head->next != NULL){
		printf("%d x %s\n",head->next->wyraz.krotnosc,head->next->wyraz.slowo);
		head=head->next;
	}
}


int main(int argc, char const *argv[])
{
	List *chyba_listy;
	readFile(&chyba_listy);
	PrintList(chyba_listy);
	return 0;
}